import copy
import math
from datetime import datetime
import numpy as np
from matplotlib import pyplot as plt
import yaml
import yamlloader
import openmdao.api as om


def delUnits(config_dict):
    '''Convert yaml strings inputs with units to float in a config dictionnary

    Args:
        config_dict (dict): input dictionnary loaded from the yaml input file

    Returns:
        dict: new_config_without_unit, same dictionnary but without the unit in the values
    '''

    new_config_without_unit = copy.deepcopy(config_dict)
    for key, value in config_dict.items():
        for key_1, value_1 in value.items():
            if type(value_1) is str:
                if '[' in value_1:
                    new_value = value_1[:-1].split('[')
                    new_config_without_unit[key][key_1] = float(new_value[0])
                else:
                    new_config_without_unit[key][key_1] = value_1
            else:
                new_config_without_unit[key][key_1] = value_1
    return new_config_without_unit

# -----------------------------------------------------------------------------
