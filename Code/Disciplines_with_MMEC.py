import openmdao.api as om
import numpy as np
import math

import yaml
import yamlloader

from functionsDatabase import delUnits


# ------------------------------------------------------------------------------------------------------------
class Energy(om.ExplicitComponent):
    """
    Computes energy consumption of the system

    Hypotheses:
        - For very early design I consider that the power consumed is proportional to the inputs
        - The time considered for the analysis is one day

    Inputs:
        - arm_movement [m]: total arm_movement in a  day
        - volume_water [m^3]: volume of water pumped by the system in a  day
        - t_sensors [s]: duration sensors on in a  day
        - i [rad]: inclination of the channels
        - length [m]: length of the channels
        - PPFD [micromol_{photons}/(s m^2)]: photosynthetic photon flux density

    Outputs:
        - energy [J]: consumed energy in a  day
    """

    def initialize(self):
        # set constants in the problem
        # load input file into config variable
        configFileName = "./input_with_MMEC.yaml"  # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader=yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)

        # design constants
        self.km = float(self.config['ENERGY_BUDGET']['km'])
        self.kl = float(self.config['ENERGY_BUDGET']['kl'])
        self.kw = float(self.config['ENERGY_BUDGET']['kw'])
        self.ks = float(self.config['ENERGY_BUDGET']['ks'])
        self.g = float(self.config['ENERGY_BUDGET']['g'])

    def setup(self):
        self.add_input('arm_movement', val=0.1)
        self.add_input('PPFD', val=200)
        self.add_input('volume_water', val=0.1)
        self.add_input('t_sensors', val=0.1)
        self.add_input('i', val=0.1)
        self.add_input('length', val=0.1)

        self.add_output('energy', val=0.0)
        self.add_output('q', val=0.0)

    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

    def compute(self, inputs, outputs):
        """

        """
        arm_movement = inputs['arm_movement']
        PPFD = inputs['PPFD']
        volume_water = inputs['volume_water']
        t_sensors = inputs['t_sensors']
        i = inputs['i']
        length = inputs['length']

        q = volume_water * np.sqrt((self.g * np.sin(i)) / (2 * length))
        outputs['q'] = q
        outputs['energy'] = self.km * arm_movement + self.kl * PPFD + self.kw * q + self.ks * t_sensors


# ------------------------------------------------------------------------------------------------------------
class Plant(om.ExplicitComponent):
    """
    Considers the plant as a discipline

    Hypotheses:
    - MMEC algorithm to compute HWCGR and then t_grow

    Inputs:
        - PPFD [micromol_{photons}/(s m^2)]: photosynthetic photon flux density
        - n_plants [-]: number of plants daily eaten by the crew
        - [arm_movement [m]: (no in this early design phase)]

    Outputs:
        - t_grow [s]: time for the plant to grow
        - n_tot_plants [-]: total number of plants grown in the system
        - [nutrients [g]: (no in this early design phase)]
    """

    def initialize(self):
        # set constants in the problem
        # load input file into config variable
        configFileName = "./input_with_MMEC.yaml"  # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader=yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)

        # design constants
        self.alpha = float(self.config['PLANT']['alpha'])
        self.CUE24 = float(self.config['PLANT']['CUE24'])
        self.A = float(self.config['PLANT']['A'])
        self.CQY = float(self.config['PLANT']['CQY'])
        self.BCF = float(self.config['PLANT']['BCF'])
        self.MW_C = float(self.config['PLANT']['MW_C'])
        self.WBF = float(self.config['PLANT']['WBF'])
        self.M_S_f = float(self.config['PLANT']['M_S_f'])
        self.H = float(self.config['PLANT']['H'])

    def setup(self):
        self.add_input('PPFD', val=200)
        self.add_input('n_plants', val=1)

        self.add_output('t_grow', val=0.0)
        self.add_output('n_tot_plants', val=1.0)
        self.add_output('HWCGR', val=0.1)
        self.add_output('HCGR', val=0.1)

    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

    def compute(self, inputs, outputs):
        """

        """
        PPFD = inputs['PPFD']
        n_plants = inputs['n_plants']

        HCG = self.alpha * PPFD * self.CUE24 * self.A *self.CQY# [mol_{carbon}/ (h m^2)] hourly carbon gain
        HCGR = (HCG * self.MW_C)/self.BCF  # [g/ (h m^2)] hourly crop growth rate
        outputs['HCGR'] = HCGR
        outputs['HWCGR'] = HCGR /(1- self.WBF)  # [g/ (h m^2)] effective hourly crop growth rate

        outputs['t_grow'] = self.M_S_f / (outputs['HWCGR'] * self.H )  # [days] time for the plant to grow
        outputs['n_tot_plants'] = n_plants * outputs['t_grow']


# ------------------------------------------------------------------------------------------------------------
class Volume(om.ExplicitComponent):
    """
    Considers the volume of the channels where the water flows of the hydroponic system as a discipline

    Version A:

    Hypotheses:
    - We consider a set of rectangular channels partially filled with water. These pipes can accommodate a variable number of plants in their length (n_y) and in their width (n_y).
    Inputs:
        - height [m]: total height of the channel
        - height_w [m]: height of the water inside the channel
        - width_material [m]: width of the material used to build the channels
        - n_pipes [-]: total number of pipes
        - n_x [-]: number of plants in the length per channel
        - n_y [-]: number of plants in the width per channel

    Outputs:
        - n_places [-]: total number of places available for plants
        - volume_water  [m^3]: total volume occupied by the water inside the channels
        - volume_material [m^3]: total volume of material used to build the channels
        - volume_occupied [m^3]: total volume occupied by the channels
        - surface_occupied [m^2]: total surface occupied by the hydropoinc system
        - length [m]: length of the channel
        - width [m]: width of the channel
    """

    def initialize(self):
        # set constants in the problem
        # load input file into config variable
        configFileName = "./input_with_MMEC.yaml"
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader=yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)

        # design constants
        self.plant_x = float(self.config['VOLUME']['plant_x'])
        self.plant_y = float(self.config['VOLUME']['plant_y'])
        self.plant_roots_x = float(self.config['VOLUME']['plant_roots_x'])
        self.plant_roots_y = float(self.config['VOLUME']['plant_roots_y'])

    def setup(self):
        self.add_input('height', val=0.1)
        self.add_input('height_w', val=0.1)
        self.add_input('width_material', val=0.01)
        self.add_input('n_pipes', val=20.0)
        self.add_input('n_x', val=1.0)
        self.add_input('n_y', val=1.0)

        self.add_output('n_places', val=1.0, shape=1)
        self.add_output('volume_water')
        self.add_output('volume_material')
        self.add_output('volume_occupied')
        self.add_output('surface_occupied')
        self.add_output('length')
        self.add_output('width')

    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

    def compute(self, inputs, outputs):
        """

        """
        height = inputs['height']
        height_w = inputs['height_w']
        width_material = inputs['width_material']
        n_pipes = inputs['n_pipes']
        n_x = inputs['n_x']
        n_y = inputs['n_y']

        outputs['n_places'] = n_pipes * n_x * n_y
        outputs['length'] = n_x * self.plant_x #length of space occupied by the plants
        length = outputs['length']
        length_pipe = length - (self.plant_x - self.plant_roots_x) # [m] length of the pipe for material calculation
        outputs['width'] = n_y * self.plant_y
        width = outputs['width']
        if n_y <= 1:
            width_pipe = self.plant_y
        else:
            width_pipe = width - (self.plant_y - self.plant_roots_y)
        outputs['volume_water'] = n_pipes * length_pipe * width_pipe * height_w
        A_material = width_pipe * height - ((width_pipe - (2 * width_material)) * (height - (2 * width_material)))
        outputs['volume_material'] = n_pipes * (length_pipe * A_material + 2*width_material*(width_pipe - (2 * width_material)) * (height - (2 * width_material)))
        outputs['surface_occupied'] = n_pipes * length * width
        outputs['volume_occupied'] = n_pipes * length * width * height


# ------------------------------------------------------------------------------------------------------------
class Crew(om.ExplicitComponent):
    """
    Considers the composition of the crew and computes the number of plants that will be eaten by the crew to meet the daily energy needs

    Hypotheses:
    - For early design we consider just one type of plant
    Inputs:
        - n_F [-]: number of women in the crew
        - n_M [-]: number of males in the crew

    Outputs:
        - n_plants [-]: number of plants required to meet the daily energy needs of the crew
    """

    def initialize(self):
        # set constants in the problem
        # load input file into config variable
        configFileName = "./input_with_MMEC.yaml"  # input file, can be changed later
        stream = open(configFileName, 'r')
        self.config = yaml.load(stream, Loader=yamlloader.ordereddict.CSafeLoader)
        stream.close()
        self.config = delUnits(self.config)

        # design constants
        self.cal_F = float(self.config['CREW']['cal_F'])
        self.cal_M = float(self.config['CREW']['cal_M'])
        self.cal_plant = float(self.config['CREW']['cal_plant'])

    def setup(self):
        self.add_input('n_F', val=0, shape=1)
        self.add_input('n_M', val=0, shape=1)

        self.add_output('n_plants', val=1, shape=1)

    def setup_partials(self):
        # Finite difference all partials.
        self.declare_partials('*', '*', method='fd')

    def compute(self, inputs, outputs):
        """

        """
        n_F = inputs['n_F']
        n_M = inputs['n_M']

        tot_calories = self.cal_F * n_F + self.cal_M * n_M
        outputs['n_plants'] = math.ceil(tot_calories / self.cal_plant)
