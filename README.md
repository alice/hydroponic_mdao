# Automated Hydroponic system Preliminary Design Optimisation

##
This work was realized during Marina Mileni Munari Research project in her Master at ISAE-SUPAERO.

```
git clone https://gitlab.isae-supaero.fr/alice/hydroponic_mdao.git
```

## Description ##
Implementation of preliminary design Multi Disciplinary Optimization (MDO) of an Hydroponic system using [OpenMDAO](https://openmdao.org/newdocs/versions/latest/main.html).

## Current Issues ## (probably solved)
- **Current Problem** : When running the Structure.py or Hydr.py files, it doesn't find the minimum of the objective function but gives the default values. The working example is the file paraboloid_optimization.py, or better Sellar_optimized.py (since it is a 2-disciplines problem) in the Training directory. 
What I tried: 
- in Structure.py I tried to simplify the objective function to minimize from 'obj = - volume_available + volume_water + volume_material' to 'obj = - volume_available' (in this case is equivalent to a maximisation of 'volume_available', which formula is '2*length * width * height'). In this case the optimization partially worked: length is fixed to 0.4(since I fixed the number of plants), height is 10 (= max allowed by constraints), but width is 0.1 instead of being infinite. I saw that I impose a limit on width (for example a value that belongs to [0.1,10]) the optimization works giving as a result width=10. I guess it could be a problem with the infinite
- in Structure.py changing the objective function to 'obj = - volume_available + volume_water' (where 'volume_water=2*length * width * height'). Minimising the objective is equivalent to maximise 'volume_available' and minimise 'volume_water'. In this case the optimization works giving as a result length=0.4, width=10, height=10, height_water=0.1.
- in Structure.py with the final objective 'obj = - volume_available + volume_water + volume_material' (where 'volume_material=2*length * width_material * height'). Now the optimization works even in this case  giving as a result length=0.4, width=10, height=10, height_water=0.1, width_material=0.1.

### Directories ##
- **Bibliography** - Bibliographic research archive. Contains all the materials used in this project.
- **Presentation and reports reviews** - Presentations and reports written for Supaero reviews.
- **OpenMDAO** - Files with code. 
    - Training : Contains tutorials with examples from [OpenMDAO website](https://openmdao.org/newdocs/versions/latest/main.html). (In particular file that has the optimization working is paraboloid_optimization.py)
    - Hydr : Directory with files with project code.
      - hydr_advanced : Directory containing implementation of MDA and MDO of a more advanced Hydroponic system model based on previous work on the Hydr project. ('Work in progress').
      - N2_diagrams: Directory containing N2 diagrams of the hydroponic system.
      - XDSM_diagrams: Directory containing XDSM diagrams of the hydroponic system.
      - _input_ : YAML file where values of constants of the model are stored. 
      - _functions_database_ : Where functions of the model are implemented.
      - _Disciplines_ : Where disciplines of the model are implemented (Energy, Plant, Structure).
      - _Hydr_ : Implementation of a MDAO of a simplified Hydroponic system model.
      - _Structure_ : Implementation of a MDAO of a Structure subgroup model.


## Quickstart guide ##
- follow the installation procedure described in part **Installation**
- Make sure you are using the right environment. To activate a specific conda environment use :
`conda activate my_env`
- run _Hydr_ (it takes the disciplines from file _Disciplines_ and group structure from the file _Structure_). As a source to compare [sellar](http://openmdao.org/newdocs/versions/latest/basic_user_guide/multidisciplinary_optimization/sellar.html).

## Installation ##
- download and install [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html) :  
`bash Anaconda-latest-Linux-x86_64.sh`
- Setup [conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) :  
`conda create -n my_env -c anaconda python=3.9.7`  
`conda activate my_env`  
`conda install pip`
- Install teh packages listed in **Dependencies**

## Dependencies ##
- _List of sofrware here_
- OpenMDAO 3.19 `pip install OpenMDAO`
- numpy 1.22.3 `conda install numpy`
- yaml 0.2.5 `conda install -c anaconda yaml`
- yamlloader 1.1.0 `pip install yamlloader`
- matplotlib 3.5.2 `pip install matplotlib`
- omxdsm 0.6 `pip install omxdsm@https://github.com/onodip/OpenMDAO-XDSM/tarball/master` (still a bit buggy)
---

## Issues ##
- **Current Problem** : When running the Structure.py or Hydr.py files, it doesn't find the minimum of the objective function but gives the default values. The working example is the file paraboloid_optimization.py in the Training directory.
- N2 generation in OpenMDAO - make sure to have the latest version of OpenMDAO (3.19). With 3.18 there are some issues with displaying the N2 matrix of a system.
- XDSM generation using [omxdsm](https://github.com/onodip/OpenMDAO-XDSM) is not 100% conform to the XDSM formalism. The documentation is not very well done, but it is expected as omxdsm is a work-in-progresss.
- ExternalCodeComp:
    - When defining `self.options['command']` to be executed by this class, use list instead of a simple string. Otherwise OpenMDAO    may encounter problem when parsing the command and checking its validity.
    - Path to files - attention on space char in the path, it may cause issues (i.e. pass the path without the leading `\` before a space or avoid using it altogether).
- Attention when accessing variables - When accessing values form `inputs/outputs['...']` (e.g. in `compute(self, inputs, outputs)`) this call returns always an array, even if the input/output is defined as a scalar.
- OpenMDAO automatically generates reports when certain execution commands are called. More details can be found in [Reports System](https://openmdao.org/newdocs/versions/latest/features/reports/reports_system.html?highlight=report)


