import openmdao.api as om
import numpy as np
import matplotlib.pyplot as plt
from omxdsm import write_xdsm
from sipbuild.generator.parser.annotations import string

from Disciplines_with_MMEC import *

plt.close('all')

w_e = np.arange(0, 1.2, 0.2)
PPFDs = []
n_xs = []
n_ys = []
n_pipess = []
energys = []
volume_materials = []
surface_occupieds = []
n_tot_plantss = []

class HydrMDA(om.Group):
    """
    Group containing the Hydroponic system MDA.
    Group containing the Hydroponic system MDA.
    """

    def setup(self):
        cycle = self.add_subsystem('cycle', om.Group(), promotes=['*'])
        cycle.add_subsystem('Crew', Crew(), promotes_inputs=['n_F', 'n_M'], promotes_outputs=['n_plants'])
        cycle.add_subsystem('Structure', Volume(),
                            promotes_inputs=['height', 'height_w', 'width_material', 'n_pipes', 'n_x', 'n_y'],
                            promotes_outputs=['n_places', 'volume_water', 'volume_material', 'volume_occupied',
                                              'surface_occupied', 'length', 'width'])
        cycle.add_subsystem('Plant', Plant(), promotes_inputs=['PPFD', 'n_plants'],
                            promotes_outputs=['HWCGR', 'HCGR', 't_grow', 'n_tot_plants'])
        cycle.add_subsystem('Automation', Energy(),
                            promotes_inputs=['arm_movement', 'PPFD', 'volume_water', 't_sensors', 'i', 'length'],
                            promotes_outputs=['energy', 'q'])

        cycle.set_input_defaults('height', 10.0)

        cycle.set_input_defaults('height_w', 9.0)
        cycle.set_input_defaults('width_material', 0.1)
        cycle.set_input_defaults('n_pipes', val=1)
        cycle.set_input_defaults('n_x', val=20)
        cycle.set_input_defaults('n_y', val=1)

        # Non Linear Block Gauss Seidel is a gradient-free solver
        cycle.nonlinear_solver = om.NonlinearBlockGS()

        self.add_subsystem('obj_cmp', om.ExecComp('obj = w_e*energy/600 + w_v*volume_material/2',
                                                  energy=0.1, volume_material=0.1),
                           promotes=['obj', 'energy','volume_material', 'w_e', 'w_v'])  # I divide by stardard values to normalize the objective function
        cycle.add_subsystem('con_cmp1', om.ExecComp('con1 = height - height_w '),
                            promotes=['con1', 'height', 'height_w'])
        cycle.add_subsystem('con_cmp2', om.ExecComp('con2 = volume_water'),
                            promotes=['con2', 'volume_water'])
        cycle.add_subsystem('con_cmp3', om.ExecComp('con3 = volume_material'),
                            promotes=['con3', 'volume_material'])
        cycle.add_subsystem('con_cmp4', om.ExecComp('con4 = n_places - n_tot_plants'),
                            promotes=['con4', 'n_places', 'n_tot_plants'])
        cycle.add_subsystem('con_cmp5', om.ExecComp(
            'con5 = width * height - ((width - (2 * width_material)) * (height - (2 * width_material)))'),
                            promotes=['con5', 'width', 'width_material', 'height'])


prob = om.Problem()
prob.model = HydrMDA()

prob.driver = om.ScipyOptimizeDriver()
prob.driver.options['optimizer'] = 'SLSQP'
prob.driver.options['maxiter'] = 10000
prob.driver.options['tol'] = 1e-8

# Recorder:
prob.driver.recording_options['includes'] = ['*']
prob.driver.recording_options['record_objectives'] = True
prob.driver.recording_options['record_constraints'] = True
prob.driver.recording_options['record_desvars'] = True
prob.driver.recording_options['record_inputs'] = True
prob.driver.recording_options['record_outputs'] = True
prob.driver.recording_options['record_residuals'] = True

prob.driver.add_recorder(om.SqliteRecorder("cases.sql"))

prob.model.add_design_var('height_w', lower=1, upper=100, ref=100)
prob.model.add_design_var('height', lower=3, upper=1000, ref=100)
prob.model.add_design_var('width_material', lower=0.01, upper=1000)
prob.model.add_design_var('arm_movement', lower=0.1, upper=100)
prob.model.add_design_var('t_sensors', lower=0.1, upper=100)
prob.model.add_design_var('i', lower=0.1, upper=1.56)  # in radians
prob.model.add_design_var('n_pipes', lower=1, upper=1000, ref=1)
prob.model.add_design_var('n_x', lower=1, upper=10000, ref=40)
prob.model.add_design_var('n_y', lower=1, upper=10000, ref=35)
prob.model.add_design_var('PPFD', lower=200, upper=1000, ref=1000)
prob.model.add_objective('obj', ref=100)
prob.model.add_constraint('con1', lower=0, upper=1000)
prob.model.add_constraint('con2', lower=0.1, upper=10000, ref=100)
prob.model.add_constraint('con3', lower=0.1, upper=10000, ref=100)
prob.model.add_constraint('con4', lower=0.1, upper=10000, ref=1000)
prob.model.add_constraint('con5', lower=0.1, upper=10000, ref=10)

# Ask OpenMDAO to finite-difference across the model to compute the gradients for the optimizer
prob.model.approx_totals()

prob.setup()
prob.set_solver_print(level=0)

prob.set_val('n_F', 3.0)
prob.set_val('n_M', 3.0)
prob.set_val('i', np.deg2rad(6))

w_e = np.arange(0, 1.1, 0.1)
for i in w_e:
    prob.set_val('w_e', i)  # Vary these values to see different trends
    prob.set_val('w_v', 1 - i)

    # run the optimization
    prob.run_driver();

    PPFDs.append(float(prob["PPFD"]))
    n_xs.append(float(prob["n_x"]))
    n_ys.append(float(prob["n_y"]))
    n_pipess.append(float(prob["n_pipes"]))
    energys.append(float(prob["energy"]))
    volume_materials.append(float(prob["volume_material"]))
    surface_occupieds.append(float(prob["surface_occupied"]))
    n_tot_plantss.append(float(prob["n_tot_plants"]))
    #

print('minimum found at')
print('t_grow', prob.get_val('t_grow'))
print('HCGR', prob.get_val('HCGR'))
print('HWCGR', prob.get_val('HWCGR'))
print('PPFD', prob.get_val('PPFD'))
print('energy', prob.get_val('energy'))
print('n_plants', prob.get_val('n_plants'))
print('n_tot_plants', prob.get_val('n_tot_plants'))
print('n_places', prob.get_val('n_places'))
print('n_pipes', prob.get_val('n_pipes'))
print('n_x', prob.get_val('n_x'))
print('n_y', prob.get_val('n_y'))
print('i', prob.get_val('i'))
print('q', prob.get_val('q'))
print('n_F', prob.get_val('n_F'))
print('n_M', prob.get_val('n_M'))
print('height_w', prob.get_val('height_w'))
print('height', prob.get_val('height'))
print('width', prob.get_val('width'))
print('length', prob.get_val('length'))
print('width_material', prob.get_val('width_material'))
print('volume_water', prob.get_val('volume_water'))
print('volume_material', prob.get_val('volume_material'))
print('volume_occupied', prob.get_val('volume_occupied'))
print('surface_occupied', prob.get_val('surface_occupied'))
print('obj', prob.get_val('obj'))
print('con1', prob.get_val('con1'))
print('con2', prob.get_val('con2'))
print('con3', prob.get_val('con3'))
print('con4', prob.get_val('con4'))
print('con5', prob.get_val('con5'))
print('arm_movement', prob.get_val('arm_movement'))
print('t_sensors', prob.get_val('t_sensors'))

# Create a figure with 8 subplots arranged in a 2x4 grid, sharing the same x-axis
fig, axs = plt.subplots(2, 4, figsize=(11, 4), sharex=True)

# Marker settings
marker_style = {'color': 'black', 'marker': 'o', 'markersize': 4}

# Plot each dataset with the specified marker style
axs[0, 0].plot(w_e, PPFDs, linestyle='None', **marker_style)
axs[0, 0].set_ylabel(r'PPFD [$\mu mol_{photons}/s \, m^2$]')
axs[0, 0].grid(True)

axs[0, 1].plot(w_e, n_xs, linestyle='None', **marker_style)
axs[0, 1].set_ylabel(r'$n_x$')
axs[0, 1].grid(True)

axs[0, 2].plot(w_e, n_ys, linestyle='None', **marker_style)
axs[0, 2].set_ylabel(r'$n_y$')
axs[0, 2].grid(True)

axs[0, 3].plot(w_e, n_pipess, linestyle='None', **marker_style)
axs[0, 3].set_ylabel(r'$n_\mathrm{pipes}$')
axs[0, 3].axis((0, 1, 0, 2))
axs[0, 3].grid(True)

axs[1, 0].plot(w_e, energys, linestyle='None', **marker_style)
axs[1, 0].set_xlabel(r'$w_e$')
axs[1, 0].set_ylabel(r'Energy [ J ]')
axs[1, 0].grid(True)

axs[1, 1].plot(w_e, volume_materials, linestyle='None', **marker_style)
axs[1, 1].set_xlabel(r'$w_e$')
axs[1, 1].set_ylabel(r'$\mathrm{Volume\ Material \, [m^3]}$')
axs[1, 1].grid(True)

axs[1, 2].plot(w_e, n_tot_plantss, linestyle='None', **marker_style)
axs[1, 2].set_xlabel(r'$w_e$')
axs[1, 2].set_ylabel(r'$n_\mathrm{tot\ plants}$')
axs[1, 2].grid(True)

axs[1, 3].plot(w_e, surface_occupieds, linestyle='None', **marker_style)
axs[1, 3].set_xlabel(r'$w_e$')
axs[1, 3].set_ylabel(r'$\mathrm{Surface\ Occupied \, [m^2]}$')
axs[1, 3].grid(True)

# Adjust layout
plt.tight_layout()

# Save the figure
plt.savefig('Graphs/combined_plots_2x4.png', dpi=300, bbox_inches='tight')

# Show the figure
plt.show()

plt.figure(figsize=(4, 4))
plt.plot(volume_materials,energys, linestyle='None', **marker_style)
plt.xlabel('$\mathrm{Volume\ Material \, [m^3]}$')
plt.ylabel('Energy [ J ]')
plt.grid(True)
plt.tight_layout()
plt.savefig('Graphs/pareto.png', dpi=300)
plt.show()

# generate N2
om.n2(prob, 'Graphs/N2_diagrams/N2-HYDR_testing_MMEC')

# generate XDSM
# Write output. PDF will only be created, if pdflatex is installed
# Error to be resolved - output of designVars* from each element of the group - that shouldn't be happening
write_xdsm(prob, filename='Graphs/XDSM_diagrams/TEST_MDA_XDSM_pyxdsm', out_format='html', show_browser=True,
           quiet=False, output_side='left', include_indepvarcomps=True)

# Print record:
prob.cleanup()
cr = om.CaseReader("cases.sql")
driver_cases = cr.list_cases('driver')

last_case = cr.get_case(driver_cases[-1])
first_case = cr.get_case(driver_cases[1])

print(first_case)
print(last_case)
